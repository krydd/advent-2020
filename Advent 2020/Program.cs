﻿using Advent_2020.Tasks;

namespace Advent_2020
{
    class Program
    {
        static void Main(string[] args)
        {
            new Day21().Go_A_and_B();
        }
    }
}
