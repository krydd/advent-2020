using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Advent_2020.Tasks
{
    public class Day9
    {
        public void Go_A()
        {
            var lines = File.ReadAllLines("Tasks\\Data\\day9.txt").Select(long.Parse).ToList();

            for (var i = 25; i < lines.Count; i++)
            {
                var found = FindPairs(lines[i], lines.Skip(i - 25).Take(25).ToList());
                if (!found)
                {
                    Console.WriteLine($"Ans: {lines[i]}");
                    break;
                }
            }
        }

        private bool FindPairs(long sum, IEnumerable<long> numbers)
        {
            var filteredNumbers = numbers.Where(n => n <= sum).ToList();

            for (var i = 0; i < filteredNumbers.Count - 1; i++)
            {
                for (var j = i + 1; j < filteredNumbers.Count; j++)
                {
                    if (filteredNumbers[i] + filteredNumbers[j] == sum)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public void Go_B()
        {
            const long target = 542529149;
            
            var lines = File.ReadAllLines("Tasks\\Data\\day9.txt").Select(long.Parse).ToList();
            
            for (var i = 0; i < lines.Count; i++)
            {
                var sum = lines[i];
                for (var j = i + 1; sum <= target; j++)
                {
                    sum += lines[j];
                    if (sum == target)
                    {
                        var contiguousRange = lines.Skip(i).Take(j - i + 1).ToList();
                        var smallest = contiguousRange.Min();
                        var largest = contiguousRange.Max();
                        Console.WriteLine($"Ans: {smallest} + {largest} = {smallest + largest}"); // 34309142 + 37477873 = 71787015 is too low
                    }
                }
            }
        }
    }
}
