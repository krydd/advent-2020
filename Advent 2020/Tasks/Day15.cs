using System;
using System.Collections.Generic;
using System.Linq;

namespace Advent_2020.Tasks
{
    public class Day15
    {
        public void Go_A()
        {
            var input = new[] {9, 6, 0, 10, 18, 2, 1};

            var history = new List<int>(input);

            var turn = input.Length + 1;
            while (turn < 2020 + 1)
            {
                var lastSpoken = history[turn - 2];
                var previousSpoken = history.Take(turn - 2).ToList();

                int next;
                if (!previousSpoken.Contains(lastSpoken))
                {
                    next = 0;
                }
                else
                {
                    var index = previousSpoken.FindLastIndex(i => i == lastSpoken);
                    next = (turn - 1) - (index + 1);
                }

                history.Add(next);
                turn++;
            }

            Console.WriteLine($"Ans: {history.Last()}");
        }

        public void Go_B()
        {
            var input = new[] {9,6,0,10,18,2,1};

            var turn = 1;
            var history = input.Take(input.Length - 1).ToDictionary(i => i, i => turn++);
            var lastSpoken = input.Last();

            while (turn < 30000000)
            {
                int nextLastSpoken;
                if (!history.ContainsKey(lastSpoken))
                {
                    nextLastSpoken = 0;
                    history.Add(lastSpoken, turn);
                }
                else
                {
                    nextLastSpoken = turn - history[lastSpoken];
                    history[lastSpoken] = turn;
                }

                lastSpoken = nextLastSpoken;
                turn++;
            }

            Console.WriteLine($"Ans: {lastSpoken}");
        }
    }
}
