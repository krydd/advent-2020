using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Advent_2020.Tasks
{
    public class Day17
    {
        public void Go_A()
        {
            var lines = File.ReadAllLines("Tasks\\Data\\day17.txt").ToList();

            var space = new Dictionary<(int x, int y, int z), bool>();
            var y = 0;
            foreach (var line in lines)
            {
                var x = 0;
                foreach (var c in line.ToCharArray())
                {
                    space.Add((x, y, 0), c != '.');
                    x++;
                }

                y++;
            }

            for (var turn = 0; turn < 6; turn++)
            {
                space = Simulate3D(space);
            }

            Console.WriteLine($"Ans: {space.Values.Count(p => p)}");
        }

        private Dictionary<(int x, int y, int z), bool> Simulate3D(IReadOnlyDictionary<(int x, int y, int z), bool> space)
        {
            var pointsToSimulate = new HashSet<(int x, int y, int z)>();
            foreach (var point in space.Keys)
            {
                pointsToSimulate.Add(point);
                GetNeighbours3D(point).ForEach(t => pointsToSimulate.Add(t));
            }

            var result = new Dictionary<(int x, int y, int z), bool>();
            foreach (var pointToCheck in pointsToSimulate)
            {
                var isInSpace = false;
                var isActive = false;
                if (space.ContainsKey(pointToCheck))
                {
                    isInSpace = true;
                    isActive = space[pointToCheck];
                }

                if (isActive)
                {
                    var activeNeighbours = GetNeighbours3D(pointToCheck).Where(space.ContainsKey).Select(t => space[t]).Count(p => p);
                    if (activeNeighbours == 2 || activeNeighbours == 3)
                    {
                        result.Add(pointToCheck, true);
                    }
                    else
                    {
                        result.Add(pointToCheck, false);
                    }
                }
                else
                {
                    var activeNeighbours = GetNeighbours3D(pointToCheck).Where(space.ContainsKey).Select(t => space[t]).Count(p => p);
                    if (activeNeighbours == 3)
                    {
                        result.Add(pointToCheck, true);
                    }
                    else if (isInSpace)
                    {
                        result.Add(pointToCheck, false);
                    }
                }
            }

            return result;
        }

        private List<(int x, int y, int z)> GetNeighbours3D((int x, int y, int z) point)
        {
            return new List<(int x, int y, int z)>
            {
                (point.x - 1, point.y - 1, point.z - 1),
                (point.x - 1, point.y - 1, point.z),
                (point.x - 1, point.y - 1, point.z + 1),
                (point.x - 1, point.y, point.z - 1),
                (point.x - 1, point.y, point.z),
                (point.x - 1, point.y, point.z + 1),
                (point.x - 1, point.y + 1, point.z - 1),
                (point.x - 1, point.y + 1, point.z),
                (point.x - 1, point.y + 1, point.z + 1),
                (point.x, point.y - 1, point.z - 1),
                (point.x, point.y - 1, point.z),
                (point.x, point.y - 1, point.z + 1),
                (point.x, point.y, point.z - 1),
                (point.x, point.y, point.z + 1),
                (point.x, point.y + 1, point.z - 1),
                (point.x, point.y + 1, point.z),
                (point.x, point.y + 1, point.z + 1),
                (point.x + 1, point.y - 1, point.z - 1),
                (point.x + 1, point.y - 1, point.z),
                (point.x + 1, point.y - 1, point.z + 1),
                (point.x + 1, point.y, point.z - 1),
                (point.x + 1, point.y, point.z),
                (point.x + 1, point.y, point.z + 1),
                (point.x + 1, point.y + 1, point.z - 1),
                (point.x + 1, point.y + 1, point.z),
                (point.x + 1, point.y + 1, point.z + 1)
            };
        }

        public void Go_B()
        {
            var lines = File.ReadAllLines("Tasks\\Data\\day17.txt").ToList();

            var space = new Dictionary<(int x, int y, int z, int w), bool>();
            var y = 0;
            foreach (var line in lines)
            {
                var x = 0;
                foreach (var c in line.ToCharArray())
                {
                    space.Add((x, y, 0, 0), c != '.');
                    x++;
                }

                y++;
            }

            for (var turn = 0; turn < 6; turn++)
            {
                space = Simulate4D(space);
            }

            Console.WriteLine($"Ans: {space.Values.Count(p => p)}");
        }

        private Dictionary<(int x, int y, int z, int w), bool> Simulate4D(Dictionary<(int x, int y, int z, int w), bool> space)
        {
            var pointsToSimulate = new HashSet<(int x, int y, int z, int w)>();
            foreach (var point in space.Keys)
            {
                pointsToSimulate.Add(point);
                GetNeighbours4D(point).ForEach(t => pointsToSimulate.Add(t));
            }

            var result = new Dictionary<(int x, int y, int z, int w), bool>();
            foreach (var pointToCheck in pointsToSimulate)
            {
                var isInSpace = false;
                var isActive = false;
                if (space.ContainsKey(pointToCheck))
                {
                    isInSpace = true;
                    isActive = space[pointToCheck];
                }

                if (isActive)
                {
                    var activeNeighbours = GetNeighbours4D(pointToCheck).Where(space.ContainsKey).Select(t => space[t]).Count(p => p);
                    if (activeNeighbours == 2 || activeNeighbours == 3)
                    {
                        result.Add(pointToCheck, true);
                    }
                    else
                    {
                        result.Add(pointToCheck, false);
                    }
                }
                else
                {
                    var activeNeighbours = GetNeighbours4D(pointToCheck).Where(space.ContainsKey).Select(t => space[t]).Count(p => p);
                    if (activeNeighbours == 3)
                    {
                        result.Add(pointToCheck, true);
                    }
                    else if (isInSpace)
                    {
                        result.Add(pointToCheck, false);
                    }
                }
            }

            return result;
        }

        private List<(int x, int y, int z, int w)> GetNeighbours4D((int x, int y, int z, int w) point)
        {
            var neighbours = new List<(int x, int y, int z, int w)>();

            for (var x = point.x - 1; x < point.x + 2; x++)
            {
                for (var y = point.y - 1; y < point.y + 2; y++)
                {
                    for (var z = point.z - 1; z < point.z + 2; z++)
                    {
                        for (var w = point.w - 1; w < point.w + 2; w++)
                        {
                            var neighbour = (x, y, z, w);
                            if (neighbour == point)
                            {
                                continue;
                            }
                            
                            neighbours.Add(neighbour);
                        }
                    }
                }
            }
            
            return neighbours;
        }
    }
}
