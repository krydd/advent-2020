using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Advent_2020.Tasks
{
    public class Day19
    {
        public void Go_A()
        {
            var lines = File.ReadAllLines("Tasks\\Data\\day19.txt").ToList();

            var rules = new Dictionary<string, string>();
            var words = new List<string>();
            
            foreach (var line in lines)
            {
                if (line == "")
                {
                    continue;
                }

                if (!line.StartsWith("a") && !line.StartsWith("b"))
                {
                    var parts = line.Split(":");
                    var rule = parts[1].Trim().Replace("\"", "");
                    rules.Add(parts[0], rule);
                }
                else
                {
                    words.Add(line);
                }
            }
            
            var zeroRule = SolveRule(rules, "0").Replace(" ", "");
            var count = words.Count(word => Regex.Match(word, $"^{zeroRule}$").Success);

            Console.WriteLine($"Ans: {count}");
        }
        
        public void Go_B()
        {
            var lines = File.ReadAllLines("Tasks\\Data\\day19b.txt").ToList();
            
            var rules = new Dictionary<string, string>();
            var words = new List<string>();
            
            foreach (var line in lines)
            {
                if (line == "")
                {
                    continue;
                }

                if (!line.StartsWith("a") && !line.StartsWith("b"))
                {
                    var parts = line.Split(":");
                    var rule = parts[1].Trim().Replace("\"", "");
                    rules.Add(parts[0], rule);
                }
                else
                {
                    words.Add(line);
                }
            }

            var i = 0;
        }

        private string SolveRule(IReadOnlyDictionary<string, string> rules, string ruleNumber)
        {
            var newRules = rules.Select(kv => kv).ToDictionary(kv => kv.Key, kv => kv.Value);
            
            while (Regex.Match(newRules[ruleNumber], "\\d+").Success)
            {
                var toReplace = Regex.Match(newRules[ruleNumber], "\\b(\\d+)\\b").Groups[1].Value;
                newRules[ruleNumber] = Regex.Replace(newRules[ruleNumber], $"\\b{toReplace}\\b", $"({newRules[toReplace]})");
            }
            
            return newRules[ruleNumber];
        }
    }
}
