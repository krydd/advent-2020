﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Advent_2020.Tasks
{
    public class Day23
    {
        public void Go_A()
        {
            const int turns = 100;
            const string input = "186524973";

            var cups = input.ToArray().Select(c => int.Parse(c.ToString())).ToList();
            var cupsList = new CircularList<int>(cups);

            var currentIndex = 0;
            var currentValue = cupsList.At(currentIndex);
            var min = cups.Min();
            var max = cups.Max();
            for (var i = 0; i < turns; i++)
            {
                var removed = cupsList.Remove(currentIndex + 1, 3);
                var destinationValue = currentValue;
                while (true)
                {
                    destinationValue--;
                    if (cupsList.Contains(destinationValue))
                    {
                        break;
                    }

                    if (destinationValue < min)
                    {
                        destinationValue = max + 1;
                    }
                }

                var destinationIndex = cupsList.IndexOf(destinationValue);
                cupsList.Add(destinationIndex + 1, removed);

                currentIndex = cupsList.IndexOf(currentValue);
                currentIndex++;
                currentValue = cupsList.At(currentIndex);
            }

            var startIndex = cupsList.IndexOf(1) + 1;
            var result = "";
            for (var i = startIndex; i < startIndex + 8; i++)
            {
                result += cupsList.At(i);
            }

            Console.WriteLine($"Ans: {result}");
        }

        private class CircularList<T> where T : struct
        {
            private readonly List<T> list = new List<T>();

            public CircularList(IEnumerable<T> input)
            {
                list.AddRange(input);
            }

            public T At(int index)
            {
                return list[FixIndex(index)];
            }

            public void Add(int index, IEnumerable<T> elements)
            {
                if (index == list.Count)
                {
                    list.AddRange(elements);
                    return;
                }

                index = FixIndex(index);

                var newList = list.Take(index).Concat(elements).Concat(list.Skip(index)).ToList();

                list.Clear();
                list.AddRange(newList);
            }

            public List<T> Remove(int index, int numElements)
            {
                if (numElements > list.Count)
                {
                    throw new Exception("Too many elements to remove!");
                }

                index = FixIndex(index);

                var numOvershot = Math.Max(0, index + numElements - (list.Count));

                var removedElements = list.Skip(index).Take(numElements - numOvershot).Concat(list.Take(numOvershot)).ToList();
                list.RemoveAll(e => removedElements.Contains(e));

                return removedElements;
            }

            private int FixIndex(int index)
            {
                while (index < 0)
                {
                    index += list.Count;
                }

                return index % list.Count;
            }

            public bool Contains(in T currentValue)
            {
                return list.Contains(currentValue);
            }

            public int IndexOf(in T currentValue)
            {
                return list.IndexOf(currentValue);
            }

            public override string ToString()
            {
                return string.Join(", ", list);
            }
        }
    }
}
