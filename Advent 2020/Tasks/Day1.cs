using System;
using System.IO;
using System.Linq;

namespace Advent_2020.Tasks
{
    public class Day1
    {
        public void Go_A()
        {
            var ints = File.ReadLines("Tasks\\Data\\day1.txt")
                .Select(int.Parse)
                .OrderBy(i => i)
                .ToArray();

            foreach (var i in ints)
            {
                foreach (var j in ints.Reverse())
                {
                    var sum = i + j;

                    if (sum == 2020)
                    {
                        Console.WriteLine($"Ans: {i}, {j} => {i * j}");
                        return;
                    }

                    if (sum < 2020)
                    {
                        break;
                    }
                }
            }
        }

        public void Go_B()
        {
            var ints = File.ReadLines("Tasks\\Data\\day1.txt")
                .Select(int.Parse)
                .OrderBy(i => i)
                .ToArray();

            foreach (var i in ints)
            {
                foreach (var j in ints)
                {
                    foreach (var k in ints.Reverse())
                    {
                        var sum = i + j + k;

                        if (sum == 2020)
                        {
                            Console.WriteLine($"Ans: {i}, {j}, {k} => {i * j * k}");
                            return;
                        }

                        if (sum < 2020)
                        {
                            break;
                        }
                    }
                }
            }
        }
    }
}
