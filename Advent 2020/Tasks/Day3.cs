using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Advent_2020.Tasks
{
    public class Day3
    {
        public void Go_A()
        {
            var lines = File.ReadAllLines("Tasks\\Data\\day3.txt");

            var grid = new char[lines.Length][];
            for (var i = 0; i < lines.Length; ++i)
            {
                grid[i] = lines[i].ToCharArray();
            }

            const int dx = 3;
            const int dy = 1;

            var sum = CalcTreesHit(grid, dx, dy);

            Console.WriteLine($"Ans: {sum}");
        }

        public void Go_B()
        {
            var lines = File.ReadAllLines("Tasks\\Data\\day3.txt");

            var grid = new char[lines.Length][];
            for (var i = 0; i < lines.Length; ++i)
            {
                grid[i] = lines[i].ToCharArray();
            }
            
            var slopes = new List<(int dx, int dy)>
            {
                (1, 1), 
                (3, 1),
                (5, 1),
                (7, 1),
                (1, 2)
            };

            var ans = slopes.Aggregate(1UL, (current, slope) => current * (ulong) CalcTreesHit(grid, slope.dx, slope.dy));

            Console.WriteLine($"Ans: {ans}");
        }

        private static int CalcTreesHit(char[][] grid, int dx, int dy)
        {
            var columns = grid[0].Length;
            
            var x = 0;
            var y = 0;

            var sum = 0;
            while (y < grid.Length)
            {
                var encounter = grid[y][x % columns];
                if (encounter == '#')
                {
                    sum++;
                }

                x += dx;
                y += dy;
            }

            return sum;
        }
    }
}
