﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Advent_2020.Tasks
{
    public class Day21
    {
        public void Go_A_and_B()
        {
            var lines = File.ReadAllLines("Tasks\\Data\\day21.txt").ToList();

            var allIngredients = new HashSet<string>();
            var allergenWithPossibleIngredients = new Dictionary<string, List<string>>();
            foreach (var line in lines)
            {
                var parts = line.Replace(")", "").Split("(contains");
                var ingredients = parts[0].Trim().Split(" ");
                var allergens = parts[1].Trim().Split(", ");
                
                foreach (var ingredient in ingredients)
                {
                    allIngredients.Add(ingredient);
                }
                
                foreach (var allergen in allergens)
                {
                    if (allergenWithPossibleIngredients.ContainsKey(allergen))
                    {
                        allergenWithPossibleIngredients[allergen] = allergenWithPossibleIngredients[allergen].Intersect(ingredients).ToList();
                    }
                    else
                    {
                        allergenWithPossibleIngredients.Add(allergen, ingredients.ToList());
                    }
                }
            }

            var confirmed = new Dictionary<string, string>();
            while (allergenWithPossibleIngredients.Any(kv => kv.Value.Count == 1 && !confirmed.ContainsKey(kv.Key)))
            {
                var (allergen, list) = allergenWithPossibleIngredients.First(kv => kv.Value.Count == 1 && !confirmed.ContainsKey(kv.Key));
                var ingredient = list.Single();
                confirmed.Add(allergen, ingredient);
                
                foreach (var (key, value) in allergenWithPossibleIngredients.Where(kv => kv.Key != allergen))
                {
                    value.Remove(ingredient);
                }
            }

            var ingredientsWithoutAllergens = allIngredients.Where(i => !confirmed.Values.Contains(i)).ToList();
            var count = lines.Sum(line => line.Split(" ").Count(ingredient => ingredientsWithoutAllergens.Contains(ingredient)));
            Console.WriteLine($"Ans A: {count}");

            var dangerList = string.Join(",", confirmed.OrderBy(kv => kv.Key).Select(kv => kv.Value));
            Console.WriteLine($"Ans B: {dangerList}");
        }
    }
}
