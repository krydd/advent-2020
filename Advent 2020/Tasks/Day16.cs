using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Advent_2020.Tasks
{
    public class Day16
    {
        public void Go_A()
        {
            var lines = File.ReadAllLines("Tasks\\Data\\day16.txt").ToList();

            var bRules = true;
            var nNearbyTickets = false;

            var rules = new HashSet<int>();
            var invalidValues = new List<int>();

            foreach (var line in lines)
            {
                switch (line)
                {
                    case "your ticket:":
                        bRules = false;
                        continue;
                    case "nearby tickets:":
                        nNearbyTickets = true;
                        continue;
                }

                if (bRules)
                {
                    foreach (var capture in Regex.Matches(line, @"\d+-\d+"))
                    {
                        var parts = (capture.ToString() ?? "").Split("-");
                        var start = int.Parse(parts[0]);
                        var end = int.Parse(parts[1]);
                        foreach (var i in Enumerable.Range(start, end - start + 1))
                        {
                            rules.Add(i);
                        }
                    }
                }

                if (nNearbyTickets)
                {
                    foreach (var ticketValue in line.Split(",").Select(int.Parse))
                    {
                        if (!rules.Contains(ticketValue))
                        {
                            invalidValues.Add(ticketValue);
                        }
                    }
                }
            }

            Console.WriteLine($"Ans: {invalidValues.Sum()}");
        }

        public void Go_B()
        {
            var lines = File.ReadAllLines("Tasks\\Data\\day16.txt").ToList();

            var bRules = true;
            var bYourTickets = false;
            var bNearbyTickets = false;

            var rules = new Dictionary<string, List<int>>();
            var ruleMisses = new Dictionary<string, HashSet<int>>();
            var yourTicket = new List<int>();

            foreach (var line in lines)
            {
                switch (line)
                {
                    case "your ticket:":
                        bRules = false;
                        bYourTickets = true;
                        continue;
                    case "nearby tickets:":
                        bYourTickets = false;
                        bNearbyTickets = true;
                        continue;
                    case "":
                        continue;
                }

                if (bRules)
                {
                    var name = line.Substring(0, line.IndexOf(":") + 1);
                    var valueRange = new List<int>();

                    foreach (var capture in Regex.Matches(line, @"\d+-\d+"))
                    {
                        var parts = (capture.ToString() ?? "").Split("-");
                        var start = int.Parse(parts[0]);
                        var end = int.Parse(parts[1]);
                        valueRange.AddRange(Enumerable.Range(start, end - start + 1));
                    }

                    rules.Add(name, valueRange);
                }

                if (bYourTickets)
                {
                    yourTicket.AddRange(line.Split(",").Select(int.Parse));
                }

                if (bYourTickets || bNearbyTickets)
                {
                    var values = line.Split(",").Select(int.Parse).ToList();
                    var skip = false;
                    foreach (var ticketValue in values)
                    {
                        if (!rules.Values.SelectMany(list => list).Contains(ticketValue))
                        {
                            skip = true;
                        }
                    }

                    if (skip)
                    {
                        continue;
                    }

                    var count = 0;
                    foreach (var ticketValue in values)
                    {
                        foreach (var (name, range) in rules)
                        {
                            if (!range.Contains(ticketValue))
                            {
                                if (!ruleMisses.ContainsKey(name))
                                {
                                    ruleMisses.Add(name, new HashSet<int>());
                                }

                                ruleMisses[name].Add(count);
                            }
                        }

                        count++;
                    }
                }
            }

            var correctPositions = new Dictionary<string, int>();

            var twenty = Enumerable.Range(0, 20);
            foreach (var (name, positions) in ruleMisses.OrderByDescending(kv => kv.Value.Count))
            {
                var pos = twenty.Where(i => !positions.Contains(i)).Single(j => !correctPositions.Values.ToList().Contains(j));
                correctPositions.Add(name, pos);
            }

            var total = correctPositions
                .Where(kv => kv.Key.StartsWith("departure"))
                .Select(kv => kv.Value)
                .Aggregate(1L, (current, departurePosition) => current * yourTicket[departurePosition]);

            Console.WriteLine($"Ans: {total}");
        }
    }
}
