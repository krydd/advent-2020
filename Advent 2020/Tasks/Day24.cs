using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Advent_2020.Tasks
{
    public class Day24
    {
        private enum Direction
        {
            East,
            SouthEast,
            SouthWest,
            West,
            NorthWest,
            NorthEast
        }

        public void Go_A()
        {
            var lines = File.ReadAllLines("Tasks\\Data\\day24.txt").ToList();

            var grid = new Dictionary<(int x, int y), bool>();

            foreach (var line in lines)
            {
                var moves = GetMovements(line);
                var pos = moves.Aggregate((0, 0), Move);

                if (grid.ContainsKey(pos))
                {
                    grid[pos] = !grid[pos];
                }
                else
                {
                    grid.Add(pos, true);
                }
            }

            Console.WriteLine($"Ans: {grid.Values.Count(v => v)}");
        }

        private (int, int) Move((int, int) pos, Direction direction)
        {
            var (x, y) = pos;

            return direction switch
            {
                Direction.East => (x + 2, y),
                Direction.SouthEast => (x + 1, y + 1),
                Direction.SouthWest => (x - 1, y + 1),
                Direction.West => (x - 2, y),
                Direction.NorthWest => (x - 1, y - 1),
                Direction.NorthEast => (x + 1, y - 1),
                _ => throw new Exception($"Unknown direction {direction}")
            };
        }

        private List<Direction> GetMovements(string line)
        {
            var result = new List<Direction>();
            for (var i = 0; i < line.Length; i++)
            {
                if (line[i] == 'e')
                {
                    result.Add(Direction.East);
                    continue;
                }

                if (line[i] == 'w')
                {
                    result.Add(Direction.West);
                    continue;
                }

                var dirString = line[i].ToString() + line[i + 1].ToString();

                switch (dirString)
                {
                    case "se":
                        result.Add(Direction.SouthEast);
                        break;
                    case "sw":
                        result.Add(Direction.SouthWest);
                        break;
                    case "nw":
                        result.Add(Direction.NorthWest);
                        break;
                    case "ne":
                        result.Add(Direction.NorthEast);
                        break;
                    default:
                        throw new Exception($"Unknown dir string: {dirString}");
                }

                i++;
            }

            return result;
        }

        public void Go_B()
        {
            var lines = File.ReadAllLines("Tasks\\Data\\day24.txt").ToList();

            var grid = new Dictionary<(int x, int y), bool>();

            foreach (var line in lines)
            {
                var moves = GetMovements(line);
                var pos = moves.Aggregate((0, 0), Move);

                if (grid.ContainsKey(pos))
                {
                    grid[pos] = !grid[pos];
                }
                else
                {
                    grid.Add(pos, true);
                }
            }

            for (int times = 0; times < 100; times++)
            {
                grid = Simulate(grid);
            }

            Console.WriteLine($"Ans: {grid.Values.Count(v => v)}");
        }

        private Dictionary<(int x, int y), bool> Simulate(IReadOnlyDictionary<(int x, int y), bool> grid)
        {
            var tilesToSimulate = new HashSet<(int, int)>();
            foreach (var tile in grid.Keys)
            {
                tilesToSimulate.Add(tile);
                GetNeighbours(tile).ForEach(t => tilesToSimulate.Add(t));
            }
            
            var result = new Dictionary<(int x, int y), bool>();

            foreach (var tileToCheck in tilesToSimulate)
            {
                var isInGrid = false;
                var isWhite = true;
                if (grid.ContainsKey(tileToCheck))
                {
                    isInGrid = true;
                    isWhite = !grid[tileToCheck];
                }

                if (isWhite)
                {
                    var blackNeighbours = GetNeighbours(tileToCheck).Where(grid.ContainsKey).Select(t => grid[t]).Count(color => color);
                    if (blackNeighbours == 2)
                    {
                        result.Add(tileToCheck, true);
                    }
                    else if (isInGrid)
                    {
                        result.Add(tileToCheck, false);
                    }
                }
                else
                {
                    var blackNeighbours = GetNeighbours(tileToCheck).Where(grid.ContainsKey).Select(t => grid[t]).Count(color => color);
                    if (blackNeighbours == 0 || blackNeighbours > 2)
                    {
                        result.Add(tileToCheck, false);
                    }
                    else
                    {
                        result.Add(tileToCheck, true);
                    }
                }
            }

            return result;
        }

        private List<(int x, int y)> GetNeighbours((int x, int y) tile)
        {
            return new List<(int x, int y)>
            {
                (tile.x + 2, tile.y),
                (tile.x + 1, tile.y + 1),
                (tile.x - 1, tile.y + 1),
                (tile.x - 2, tile.y),
                (tile.x + 1, tile.y - 1),
                (tile.x - 1, tile.y - 1)
            };
        }
    }
}
