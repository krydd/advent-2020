using System;
using System.IO;
using System.Linq;

namespace Advent_2020.Tasks
{
    public class Day18
    {
        private enum Operator
        {
            Plus,
            Multiplication
        }

        public void Go_A()
        {
            var lines = File.ReadAllLines("Tasks\\Data\\day18.txt").ToList();

            var total = lines.Select(s => SolveA(s, 0).Item1).Aggregate(0L, (a, b) => a + b);

            Console.WriteLine($"Ans: {total}");
        }

        private (long sum, int index) SolveA(string equation, int indexIn)
        {
            var sum = 0L;
            var op = Operator.Plus;
            var i = indexIn;
            for (; i < equation.ToCharArray().Length; i++)
            {
                var token = equation.ToCharArray()[i];

                if (token == ' ')
                {
                    continue;
                }

                if (token == '*')
                {
                    op = Operator.Multiplication;
                    continue;
                }

                if (token == '+')
                {
                    op = Operator.Plus;
                    continue;
                }

                long newValue;
                switch (token)
                {
                    case '(':
                        (newValue, i) = SolveA(equation, i + 1);
                        break;
                    case ')':
                        return (sum, i);
                    default:
                        newValue = long.Parse(token.ToString());
                        break;
                }

                switch (op)
                {
                    case Operator.Plus:
                        sum += newValue;
                        break;
                    case Operator.Multiplication:
                        sum *= newValue;
                        break;
                    default:
                        throw new Exception($"Unknown operator {op.ToString()}");
                }
            }

            return (sum, i);
        }

        public void Go_B()
        {
            var lines = File.ReadAllLines("Tasks\\Data\\day18.txt").ToList();

            var total = lines.Select(s => SolveA(s, 0).Item1).Aggregate(0L, (a, b) => a + b);

            Console.WriteLine($"Ans: {total}");
        }
    }
}
