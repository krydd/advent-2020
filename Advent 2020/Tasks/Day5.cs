using System;
using System.IO;
using System.Linq;

namespace Advent_2020.Tasks
{
    public class Day5
    {
        public void Go_A()
        {
            var maxSeatId = File.ReadAllLines("Tasks\\Data\\day5.txt").Select(GetSeatId).Max();

            Console.WriteLine($"Ans: {maxSeatId}");
        }
        
        public void Go_B()
        {
            var allSeatIds = File.ReadAllLines("Tasks\\Data\\day5.txt").Select(GetSeatId).OrderBy(i => i).ToList();

            var lastId = 0;
            foreach (var seatId in allSeatIds)
            {
                if (seatId - lastId == 2)
                {
                    Console.WriteLine($"Ans: {seatId - 1}");
                    break;
                }
            
                lastId = seatId;
            }
        }

        private int GetSeatId(string line)
        {
            var minRow = 0;
            var maxRow = 127;
            var minCol = 0;
            var maxCol = 7;

            foreach (var c in line.ToCharArray())
            {
                switch (c)
                {
                    case 'F':
                        maxRow -= (maxRow - minRow) / 2 + 1;
                        break;
                    case 'B':
                        minRow += (maxRow - minRow) / 2 + 1;
                        break;
                    case 'L':
                        maxCol -= (maxCol - minCol) / 2 + 1;
                        break;
                    case 'R':
                        minCol += (maxCol - minCol) / 2 + 1;
                        break;
                }
            }

            return maxRow * 8 + maxCol;
        }
    }
}
