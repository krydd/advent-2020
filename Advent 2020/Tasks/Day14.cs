using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Advent_2020.Tasks
{
    public class Day14
    {
        public void Go_A()
        {
            var lines = File.ReadAllLines("Tasks\\Data\\day14.txt").ToList();

            var mask = "";
            var memory = new Dictionary<ulong, ulong>();

            foreach (var line in lines)
            {
                if (line.StartsWith("mask"))
                {
                    mask = line.Split(" ").Last();
                }
                else
                {
                    var result = Regex.Match(line, @"mem\[(\d+)\] = (\d+).*");
                    var address = ulong.Parse(result.Groups[1].Value);
                    var valueBeforeMask = ulong.Parse(result.Groups[2].Value);

                    memory[address] = ApplyMask_A(valueBeforeMask, mask);
                }
            }

            Console.WriteLine($"Ans: {memory.Values.Aggregate((a, b) => a + b)}");
        }

        private ulong ApplyMask_A(ulong value, string mask)
        {
            var position = 0;
            foreach (var c in mask.ToCharArray().Reverse())
            {
                if (c == '0')
                {
                    value = SetBitTo0(value, position);
                }

                if (c == '1')
                {
                    value = SetBitTo1(value, position);
                }

                position++;
            }

            return value;
        }

        public void Go_B()
        {
            var lines = File.ReadAllLines("Tasks\\Data\\day14.txt").ToList();

            var mask = "";
            var memory = new Dictionary<ulong, ulong>();

            foreach (var line in lines)
            {
                if (line.StartsWith("mask"))
                {
                    mask = line.Split(" ").Last();
                }
                else
                {
                    var result = Regex.Match(line, @"mem\[(\d+)\] = (\d+).*");
                    var addressBeforeMask = ulong.Parse(result.Groups[1].Value);
                    var value = ulong.Parse(result.Groups[2].Value);

                    var addresses = GetAddresses(addressBeforeMask, mask);

                    foreach (var address in addresses)
                    {
                        memory[address] = value;
                    }
                }
            }

            Console.WriteLine($"Ans: {memory.Values.Aggregate((a, b) => a + b)}");
        }

        private IEnumerable<ulong> GetAddresses(ulong value, string mask)
        {
            var position = 0;
            foreach (var c in mask.ToCharArray().Reverse())
            {
                if (c == '1')
                {
                    value = SetBitTo1(value, position);
                }

                position++;
            }

            var allAddresses = new List<ulong> {value};
            position = 0;
            foreach (var c in mask.ToCharArray().Reverse())
            {
                var temp = new List<ulong>();
                
                if (c == 'X')
                {
                    temp.AddRange(allAddresses.Select(
                        address => IsBitSetTo1(address, position) ? SetBitTo0(address, position) : SetBitTo1(address, position)));
                }

                allAddresses.AddRange(temp);
                position++;
            }

            return allAddresses;
        }

        private ulong SetBitTo1(ulong value, int position)
        {
            return value | 1UL << position;
        }

        private ulong SetBitTo0(ulong value, int position)
        {
            return value & ~(1UL << position);
        }

        private bool IsBitSetTo1(ulong value, int position)
        {
            return (value & (1UL << position)) != 0;
        }
    }
}
