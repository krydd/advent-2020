using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Advent_2020.Tasks
{
    public class Day10
    {
        public void Go_A()
        {
            var lines = File.ReadAllLines("Tasks\\Data\\day10.txt").Select(int.Parse).OrderBy(i => i).ToList();

            var (_, oneJumps, threeJumps) = lines.Aggregate((0, 0, 0), (values, nextJolt) =>
            {
                var previousJolt = values.Item1;
                var totalOneJumps = values.Item2;
                var totalThreeJumps = values.Item3;

                var diff = nextJolt - previousJolt;
                if (diff == 1)
                {
                    totalOneJumps++;
                }
                else if (diff == 3)
                {
                    totalThreeJumps++;
                }

                return (nextJolt, totalOneJumps, totalThreeJumps);
            });

            threeJumps += 1; // Built-in adapter

            Console.WriteLine($"Ans: {oneJumps} * {threeJumps} = {oneJumps * threeJumps}");
        }

        public void Go_B()
        {
            var lines = File.ReadAllLines("Tasks\\Data\\day10.txt").Select(int.Parse).ToList();
            var maxValue = lines.Max() + 3;
            
            lines.AddRange(new[] {0, maxValue});
            lines.Sort();

            var nodes = new List<Node>();
            for (var i = 0; i < lines.Count; i++)
            {
                var currentValue = lines[i];
                var currentNode = GetOrAddNode(nodes, currentValue);

                var nextIndex = i + 1;
                if (nextIndex == lines.Count)
                {
                    continue;
                }
                var nextValue = lines[nextIndex];
                while (nextValue < currentValue + 4 && nextIndex != lines.Count)
                {
                    var neighbour = GetOrAddNode(nodes, nextValue);
                    
                    currentNode.Neighbours.Add(neighbour);

                    nextIndex++;
                    if (nextIndex == lines.Count)
                    {
                        break;
                    }
                    nextValue = lines[nextIndex];
                }
            }
            
            foreach (var node in nodes.OrderByDescending(n => n.Value))
            {
                node.Count = node.Value == maxValue ? 1 : node.Neighbours.Select(n => n.Count).Sum();
            }

            Console.WriteLine($"Ans: {nodes.First().Count}");
        }

        private Node GetOrAddNode(ICollection<Node> nodes, int value)
        {
            var currentNode = nodes.FirstOrDefault(node => node.Value == value);
            if (currentNode == null)
            {
                currentNode = new Node(value);
                nodes.Add(currentNode);
            }

            return currentNode;
        }

        private class Node
        {
            public Node(int value)
            {
                Value = value;
            }

            public int Value { get; }
            public long Count { get; set; }
            public List<Node> Neighbours { get; } = new List<Node>();

            public override string ToString()
            {
                return $"V:{Value} C:{Count} - N:{string.Join(",", Neighbours.Select(n => n.Value))}";
            }
        }
    }
}
