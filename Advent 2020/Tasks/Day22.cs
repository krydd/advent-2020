﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Advent_2020.Tasks
{
    public class Day22
    {
        public void Go_A()
        {
            var player1 = new Queue<int>(new[] {10, 39, 16, 32, 5, 46, 47, 45, 48, 26, 36, 27, 24, 37, 49, 25, 30, 13, 23, 1, 9, 3, 31, 14, 4});
            var player2 = new Queue<int>(new[] {2, 15, 29, 41, 11, 21, 8, 44, 38, 19, 12, 20, 40, 17, 22, 35, 34, 42, 50, 6, 33, 7, 18, 28, 43});

            while (player1.Count != 0 && player2.Count != 0)
            {
                var p1Card = player1.Dequeue();
                var p2Card = player2.Dequeue();

                if (p1Card > p2Card)
                {
                    player1.Enqueue(p1Card);
                    player1.Enqueue(p2Card);
                }
                else
                {
                    player2.Enqueue(p2Card);
                    player2.Enqueue(p1Card);
                }
            }

            var winningPlayer = player1.Count == 0 ? player2.ToList() : player1.ToList();

            var multi = 1;
            winningPlayer.Reverse();
            var score = winningPlayer.Aggregate(0, (a, b) => a + b * multi++);

            Console.WriteLine($"Ans: {score}");
        }

        public void Go_B()
        {
            var p1StartingDeck = new[] {10, 39, 16, 32, 5, 46, 47, 45, 48, 26, 36, 27, 24, 37, 49, 25, 30, 13, 23, 1, 9, 3, 31, 14, 4};
            var p2StartingDeck = new[] {2, 15, 29, 41, 11, 21, 8, 44, 38, 19, 12, 20, 40, 17, 22, 35, 34, 42, 50, 6, 33, 7, 18, 28, 43};
            var game = new Game(p1StartingDeck, p2StartingDeck);

            var (_, winningPlayer) = game.Play();
            winningPlayer.Reverse();
            var multi = 1;
            var score = winningPlayer.Aggregate(0, (a, b) => a + b * multi++);

            Console.WriteLine($"Ans: {score}");
        }

        private class Game
        {
            internal enum Player
            {
                Player1,
                Player2
            }

            private readonly Queue<int> player1;
            private readonly Queue<int> player2;
            private readonly List<string> history = new List<string>();

            public Game(IEnumerable<int> p1, IEnumerable<int> p2)
            {
                player1 = new Queue<int>(p1);
                player2 = new Queue<int>(p2);
            }

            public (Player, List<int>) Play()
            {
                while (true)
                {
                    var historyString = string.Join(",", player1) + ";" + string.Join(",", player2);
                    
                    if (history.Any(h => h == historyString))
                    {
                        Console.WriteLine("Dup found!");
                        return (Player.Player1, player1.ToList());
                    }

                    history.Add(historyString);

                    var p1Card = player1.Dequeue();
                    var p2Card = player2.Dequeue();

                    if (player1.Count >= p1Card && player2.Count >= p2Card)
                    {
                        var subGame = new Game(player1.Take(p1Card).ToList(), player2.Take(p2Card).ToList());
                        var result = subGame.Play();
                        if (result.Item1 == Player.Player1)
                        {
                            player1.Enqueue(p1Card);
                            player1.Enqueue(p2Card);
                        }
                        else
                        {
                            player2.Enqueue(p2Card);
                            player2.Enqueue(p1Card);
                        }
                    }
                    else
                    {
                        if (p1Card > p2Card)
                        {
                            player1.Enqueue(p1Card);
                            player1.Enqueue(p2Card);
                        }
                        else
                        {
                            player2.Enqueue(p2Card);
                            player2.Enqueue(p1Card);
                        }
                    }

                    if (!player1.Any())
                    {
                        return (Player.Player2, player2.ToList());
                    }

                    if (!player2.Any())
                    {
                        return (Player.Player1, player1.ToList());
                    }
                }
            }
        }
    }
}
