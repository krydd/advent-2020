﻿using System;

namespace Advent_2020.Tasks
{
    public class Day25
    {
        public void Go_A()
        {
            const int cardPublicKey = 17773298;
            const int doorPublicKey = 15530095;

            const int cardSubjectNumber = 7;
            const int doorSubjectNumber = 7;

            var cardLoops = FindLoopConstant(cardPublicKey, cardSubjectNumber);
            var doorLoops = FindLoopConstant(doorPublicKey, doorSubjectNumber);

            var encryptionKey = FindEncryptionKey(doorPublicKey, cardLoops);

            Console.WriteLine($"Ans: {encryptionKey}");
        }

        private int FindLoopConstant(int publicKey, int subjectNumber)
        {
            var loops = 0;
            var test = 1;
            while (test != publicKey)
            {
                test *= subjectNumber;
                test %= 20201227;
                loops++;
            }

            return loops;
        }

        private int FindEncryptionKey(in int publicKey, in int loops)
        {
            var ans = 1L;
            for (var i = 0; i < loops; i++)
            {
                ans *= publicKey;
                ans %= 20201227;
            }

            return (int) ans;
        }
    }
}
