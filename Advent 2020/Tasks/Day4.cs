using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Advent_2020.Tasks
{
    public class Day4
    {
        private class Passport
        {
            private long? byr { get; set; } // (Birth Year)
            private long? iyr { get; set; } // (Issue Year)
            private long? eyr { get; set; } // (Expiration Year)
            private string hgt { get; set; } // (Height)
            private string hcl { get; set; } // (Hair Color)
            private string ecl { get; set; } // (Eye Color)
            private string pid { get; set; } // (Passport ID)
            private long? cid { get; set; } // (Country ID)

            public bool IsValid_A()
            {
                return byr != null &&
                       iyr != null &&
                       eyr != null &&
                       hgt != null &&
                       hcl != null &&
                       ecl != null &&
                       pid != null;
            }

            public bool IsValid_B()
            {
                return byr != null && 1920 <= byr && byr <= 2002 &&
                       iyr != null && 2010 <= iyr && iyr <= 2020 &&
                       eyr != null && 2020 <= eyr && eyr <= 2030 &&
                       hgt != null && IsValidHeight(hgt) &&
                       hcl != null && IsValidHairColor(hcl) &&
                       ecl != null && IsValidEyeColor(ecl) &&
                       pid != null && IsValidPassportId(pid);
            }

            private bool IsValidHeight(string heightString)
            {
                var match = Regex.Match(heightString, @"(\d+)\w*");
                var height = int.Parse(match.Groups[1].Value);
                if (heightString.EndsWith("cm"))
                {
                    return 150 <= height && height <= 193;
                }

                if (heightString.EndsWith("in"))
                {
                    return 59 <= height && height <= 76;
                }

                return false;
            }

            private bool IsValidHairColor(string hairColorString)
            {
                return Regex.Match(hairColorString, @"#[0-9a-f]{6}").Success;
            }

            private bool IsValidEyeColor(string eyeColorString)
            {
                var validColors = new List<string>
                {
                    "amb", "blu", "brn", "gry", "grn", "hzl", "oth"
                };

                return validColors.Contains(eyeColorString);
            }

            private bool IsValidPassportId(string passportId)
            {
                if (passportId.Length != 9)
                {
                    return false;
                }

                return long.TryParse(passportId, out _);
            }

            public void SetValue(string key, string value)
            {
                try
                {
                    switch (key)
                    {
                        case "byr":
                            byr = long.Parse(value);
                            break;
                        case "iyr":
                            iyr = long.Parse(value);
                            break;
                        case "eyr":
                            eyr = long.Parse(value);
                            break;
                        case "hgt":
                            hgt = value;
                            break;
                        case "hcl":
                            hcl = value;
                            break;
                        case "ecl":
                            ecl = value;
                            break;
                        case "pid":
                            pid = value;
                            break;
                        case "cid":
                            cid = long.Parse(value);
                            break;
                        default:
                            throw new Exception($"Unknown key: {key} with value {value}");
                    }
                }
                catch (FormatException fe)
                {
                    Console.WriteLine($"Could not parse key:'{key}' with value:'{value}'.\nMessage: {fe.Message}");
                    Environment.Exit(-1);
                }
            }
        }

        public void Go_A()
        {
            var allPassports = ParseAllPassports("Tasks\\Data\\day4.txt");

            Console.WriteLine($"Ans: {allPassports.Count(p => p.IsValid_A())}");
        }

        public void Go_B()
        {
            var allPassports = ParseAllPassports("Tasks\\Data\\day4.txt");

            Console.WriteLine($"Ans: {allPassports.Count(p => p.IsValid_B())}");
        }

        private static List<Passport> ParseAllPassports(string fileName)
        {
            var allPassports = new List<Passport>();

            Passport currentPassport = null;
            foreach (var line in File.ReadAllLines(fileName))
            {
                if (line == "")
                {
                    allPassports.Add(currentPassport);
                    currentPassport = null;
                    continue;
                }

                currentPassport ??= new Passport();

                var parts = line.Split(" ");
                foreach (var part in parts)
                {
                    var key = part.Split(":")[0];
                    var value = part.Split(":")[1];
                    currentPassport.SetValue(key, value);
                }
            }

            if (currentPassport != null)
            {
                allPassports.Add(currentPassport);
            }

            return allPassports;
        }
    }
}
