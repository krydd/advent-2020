using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Advent_2020.Tasks
{
    public class Day7
    {
        public void Go_A()
        {
            var lines = File.ReadAllLines("Tasks\\Data\\day7.txt");

            var tree = new Dictionary<string, List<string>>();

            foreach (var line in lines)
            {
                var result = Regex.Match(line, @"([\w ]+) bags contain ([\w\d ,]+)");
                var from = result.Groups[1].Value;

                var tos = new List<string>();
                if (result.Groups[2].Value == "no other bags")
                {
                    // No tos
                }
                else
                {
                    tos = result.Groups[2].Value.Split(",").Select(t => t.Trim()).Select(to => to.Substring(2, to.IndexOf("bag") - 3)).ToList();
                }

                foreach (var to in tos)
                {
                    if (tree.ContainsKey(to))
                    {
                        tree[to].Add(from);
                    }
                    else
                    {
                        tree.Add(to, new List<string> {from});
                    }
                }
            }

            var output = new HashSet<string>();
            AddBags(tree, output, "shiny gold");

            Console.WriteLine($"Ans: {output.Count}"); // 109 is too high
        }

        private void AddBags(IReadOnlyDictionary<string, List<string>> tree, ISet<string> output, string bagName)
        {
            if (!tree.ContainsKey(bagName))
            {
                return;
            }

            foreach (var bn in tree[bagName])
            {
                output.Add(bn);
            }

            tree[bagName].ForEach(bn => AddBags(tree, output, bn));
        }

        public void Go_B()
        {
            var lines = File.ReadAllLines("Tasks\\Data\\day7.txt");

            var tree = new Dictionary<string, List<(int, string)>>();

            foreach (var line in lines)
            {
                // mirrored turquoise bags contain 2 dim crimson bags, 4 clear crimson bags, 1 dotted blue bag.
                var result = Regex.Match(line, @"([\w ]+) bags contain ([\w\d ,]+)");
                var from = result.Groups[1].Value;

                var tos = new List<(int, string)>();
                if (result.Groups[2].Value == "no other bags")
                {
                    // No tos
                }
                else
                {
                    tos = result.Groups[2].Value.Split(",").Select(t => t.Trim()).Select(to => (int.Parse(to.First().ToString()), to.Substring(2, to.IndexOf("bag") - 3))).ToList();
                }

                tree.Add(from, tos.ToList());
            }

            var total = CountBags(tree, "shiny gold") - 1;

            Console.WriteLine($"Ans: {total}");
        }

        private int CountBags(IReadOnlyDictionary<string, List<(int, string)>> tree, string bagName)
        {
            var total = 1;
            foreach (var (times, name) in tree[bagName])
            {
                total += times * CountBags(tree, name);
            }

            return total;
        }
    }
}
