using System;
using System.Collections.Generic;
using System.IO;

namespace Advent_2020.Tasks
{
    public class Day8
    {
        public void Go_A()
        {
            var lines = File.ReadAllLines("Tasks\\Data\\day8.txt");

            var visitedLines = new List<int>();
            
            var current = 0;
            var acc = 0;
            
            while (true)
            {
                if (visitedLines.Contains(current))
                {
                    Console.WriteLine($"Ans: {acc}");
                    break;
                }
                
                visitedLines.Add(current);

                var parts = lines[current].Split(" ");
                var op = parts[0];
                var argument = int.Parse(parts[1]);
                switch (op)
                {
                    case "acc":
                        acc += argument;
                        current++;
                        break;
                    case "jmp":
                        current += argument;
                        break;
                    case "nop":
                        current++;
                        break;
                    default:
                        throw new Exception($"Unknown op {op} with arg {argument}");
                }
            }
        }

        public void Go_B()
        {
            var lines = File.ReadAllLines("Tasks\\Data\\day8.txt");

            var visitedLines = new List<int>();

            var current = 0;
            var acc = 0;
            var changed = ChangeNextOp(lines, -1);
            
            while (current != lines.Length)
            {
                if (visitedLines.Contains(current))
                {
                    current = 0;
                    acc = 0;
                    visitedLines.Clear();
                    changed = ChangeNextOp(lines, changed);
                }
                
                visitedLines.Add(current);

                var parts = lines[current].Split(" ");
                var op = parts[0];
                var argument = int.Parse(parts[1]);
                switch (op)
                {
                    case "acc":
                        acc += argument;
                        current++;
                        break;
                    case "jmp":
                        current += argument;
                        break;
                    case "nop":
                        current++;
                        break;
                    default:
                        throw new Exception($"Unknown op {op} with arg {argument}");
                }
            }
            
            Console.WriteLine($"Ans: {acc}");
        }

        private int ChangeNextOp(IList<string> program, int changed)
        {
            if (changed != -1)
            {
                program[changed] = FlipOp(program[changed]);
            }

            for (var i = changed + 1; i < program.Count; i++)
            {
                if (program[i].StartsWith("nop") || program[i].StartsWith("jmp"))
                {
                    program[i] = FlipOp(program[i]);
                    return i;
                }
            }
            
            throw new Exception("Did not find operation to change.");
        }

        private string FlipOp(string opAndArgument)
        {
            var parts = opAndArgument.Split(" ");
            var newOp = parts[0] == "jmp" ? "nop" : "jmp";

            return $"{newOp} {parts[1]}";
        }
    }
}
