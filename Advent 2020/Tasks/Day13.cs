using System;
using System.Collections.Generic;
using System.Linq;

namespace Advent_2020.Tasks
{
    public class Day13
    {
        private const string BusesInput = "23,x,x,x,x,x,x,x,x,x,x,x,x,41,x,x,x,x,x,x,x,x,x,647,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,13,19,x,x,x,x,x,x,x,x,x,29,x,557,x,x,x,x,x,37,x,x,x,x,x,x,x,x,x,x,17";
        
        public void Go_A()
        {
            const int timeStamp = 1006726;

            var busIds = BusesInput.Split(",").Where(s => !s.Contains("x")).Select(int.Parse).ToList();

            var (busId, waitTime) = busIds
                .Select(bid =>
                {
                    var rest = timeStamp % bid;
                    return rest == 0 ? (bid, waitTime: 0) : (bid, waitTime: bid - rest);
                })
                .OrderBy(b => b.waitTime).First();

            Console.WriteLine($"Ans: {busId} * {waitTime} = {busId * waitTime}");
        }

        public void Go_B()
        {
            var busesAndTimes = new Dictionary<long, long>();

            var time = 0L;
            foreach (var busId in BusesInput.Split(","))
            {
                if (busId != "x")
                {
                    busesAndTimes.Add(long.Parse(busId), time);
                }

                time++;
            }

            var offset = busesAndTimes.Keys.First();
            var checkingBusNr = 1;
            var currentTime = 0L;
            while (true)
            {
                var busId = busesAndTimes.Keys.ElementAt(checkingBusNr);
                var timeOffset = busesAndTimes.Values.ElementAt(checkingBusNr);
                if ((currentTime + timeOffset) % busId == 0)
                {
                    if (checkingBusNr + 1 == busesAndTimes.Count)
                    {
                        break;
                    }
                    offset = Lcm(busesAndTimes.Keys.Take(1 + checkingBusNr));
                    checkingBusNr++;
                }

                currentTime += offset;
            }

            Console.WriteLine($"Offset: {currentTime}");
        }

        private long Lcm(IEnumerable<long> longs)
        {
            return longs.Aggregate((a, b) => a * b);
        }
    }
}
