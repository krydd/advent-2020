using System;
using System.IO;
using System.Linq;

namespace Advent_2020.Tasks
{
    public class Day12
    {
        public void Go_A()
        {
            var lines = File.ReadAllLines("Tasks\\Data\\day12.txt").ToList();

            var heading = 0;
            var shipX = 0;
            var shipY = 0;

            foreach (var line in lines)
            {
                var command = line[0];
                var value = int.Parse(line.Substring(1));

                switch (command)
                {
                    case 'N':
                        shipY += value;
                        break;
                    case 'S':
                        shipY -= value;
                        break;
                    case 'E':
                        shipX += value;
                        break;
                    case 'W':
                        shipX -= value;
                        break;
                    case 'L':
                        heading = Turn(heading, value);
                        break;
                    case 'R':
                        heading = Turn(heading, -value);
                        break;
                    case 'F':
                        switch (heading)
                        {
                            case 0:
                                shipX += value;
                                break;
                            case 90:
                                shipY += value;
                                break;
                            case 180:
                                shipX -= value;
                                break;
                            case 270:
                                shipY -= value;
                                break;
                            default:
                                throw new Exception($"Unknown heading {heading}");
                        }
                        break;
                    default:
                        throw new Exception($"Unknown command {command}");
                }
            }

            Console.WriteLine($"Ans: Abs({shipX}) + Abs({shipY}) = {Math.Abs(shipX) + Math.Abs(shipY)}");
        }

        private int Turn(int heading, int value)
        {
            var newHeading = (heading + value) % 360;
            return newHeading < 0 ? newHeading + 360 : newHeading;
        }

        public void Go_B()
        {
            var lines = File.ReadAllLines("Tasks\\Data\\day12.txt").ToList();

            var shipX = 0;
            var shipY = 0;
            
            var waypointX = 10;
            var waypointY = 1;
            
            foreach (var line in lines)
            {
                var command = line[0];
                var value = int.Parse(line.Substring(1));

                switch (command)
                {
                    case 'N':
                        waypointY += value;
                        break;
                    case 'S':
                        waypointY -= value;
                        break;
                    case 'E':
                        waypointX += value;
                        break;
                    case 'W':
                        waypointX -= value;
                        break;
                    case 'L':
                        (waypointX, waypointY) = RotateWaypoint(value, waypointX, waypointY);
                        break;
                    case 'R':
                        (waypointX, waypointY) = RotateWaypoint(-value, waypointX, waypointY);
                        break;
                    case 'F':
                        shipX += value * waypointX;
                        shipY += value * waypointY;
                        break;
                    default:
                        throw new Exception($"Unknown command {command}");
                }
            }
            
            Console.WriteLine($"Ans: Abs({shipX}) + Abs({shipY}) = {Math.Abs(shipX) + Math.Abs(shipY)}");
        }

        private (int newWaypointX, int newWaypointY) RotateWaypoint(int degrees, int waypointX, int waypointY)
        {
            degrees = (degrees + (degrees < 0 ? 360 : 0)) % 360;
            return degrees switch
            {
                0 => (waypointX, waypointY),
                90 => (-waypointY, waypointX),
                180 => (-waypointX, -waypointY),
                270 => (waypointY, -waypointX),
                _ => throw new Exception($"Unknown degree value {degrees}")
            };
        }
    }
}
