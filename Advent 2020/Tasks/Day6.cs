using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Advent_2020.Tasks
{
    public class Day6
    {
        public void Go_A()
        {
            var total = 0;
            var chars = new List<char>();
            foreach (var line in File.ReadAllLines("Tasks\\Data\\day6.txt"))
            {
                if (line == "")
                {
                    total += chars.Count;
                    chars.Clear();
                    continue;
                }

                foreach (var c in line.ToCharArray())
                {
                    if (!chars.Contains(c))
                    {
                        chars.Add(c);
                    }
                }
            }

            total += chars.Count;

            Console.WriteLine($"Ans: {total}");
        }
        
        public void Go_B()
        {
            var first = true;
            var total = 0;
            var chars = new List<char>();
            foreach (var line in File.ReadAllLines("Tasks\\Data\\day6.txt"))
            {
                if (line == "")
                {
                    total += chars.Count;
                    chars.Clear();
                    first = true;
                    continue;
                }

                if (first)
                {
                    chars.AddRange(line.ToCharArray());
                    first = false;
                    continue;
                }

                chars = line.ToCharArray().Intersect(chars).ToList();
            }

            total += chars.Count;

            Console.WriteLine($"Ans: {total}");
        }
    }
}
