using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Advent_2020.Tasks
{
    public class Day11
    {
        public void Go_A()
        {
            var lines = File.ReadAllLines("Tasks\\Data\\day11.txt").ToList();
            var cols = lines.First().Length;
            var rows = lines.Count;

            var grid = new char[cols, rows];
            for (var x = 0; x < cols; x++)
            {
                for (var y = 0; y < rows; y++)
                {
                    grid[x, y] = lines[y][x];
                }
            }

            var changed = true;
            while (changed)
            {
                grid = Simulate(grid, NumberOfAdjacentOccupied_A, 4, out changed);
            }

            var totalSeats = grid.Cast<char>().Count(c => c == '#');
            Console.WriteLine($"Ans: {totalSeats}");
        }

        public void Go_B()
        {
            var lines = File.ReadAllLines("Tasks\\Data\\day11.txt").ToList();
            var cols = lines.First().Length;
            var rows = lines.Count;

            var grid = new char[cols, rows];
            for (var x = 0; x < cols; x++)
            {
                for (var y = 0; y < rows; y++)
                {
                    grid[x, y] = lines[y][x];
                }
            }

            var changed = true;
            while (changed)
            {
                grid = Simulate(grid, NumberOfAdjacentOccupied_B, 5, out changed);
            }

            var totalSeats = grid.Cast<char>().Count(c => c == '#');
            Console.WriteLine($"Ans: {totalSeats}");
        }

        private char[,] Simulate(char[,] grid, Func<int, int, char[,], int> adjacentOccupiedFunction, int toEmptyTolerance, out bool changed)
        {
            var cols = grid.GetLength(0);
            var rows = grid.GetLength(1);
            changed = false;

            var output = new char [cols, rows];
            for (var x = 0; x < cols; x++)
            {
                for (var y = 0; y < rows; y++)
                {
                    if (grid[x, y] == 'L')
                    {
                        if (adjacentOccupiedFunction(x, y, grid) == 0)
                        {
                            output[x, y] = '#';
                            changed = true;
                        }
                        else
                        {
                            output[x, y] = 'L';
                        }
                    }
                    else if (grid[x, y] == '#')
                    {
                        if (adjacentOccupiedFunction(x, y, grid) >= toEmptyTolerance)
                        {
                            output[x, y] = 'L';
                            changed = true;
                        }
                        else
                        {
                            output[x, y] = '#';
                        }
                    }
                    else
                    {
                        output[x, y] = grid[x, y];
                    }
                }
            }

            return output;
        }

        private int NumberOfAdjacentOccupied_A(int midX, int midY, char[,] grid)
        {
            var cols = grid.GetLength(0);
            var rows = grid.GetLength(1);

            var total = 0;
            for (var x = midX - 1; x <= midX + 1; x++)
            {
                for (var y = midY - 1; y <= midY + 1; y++)
                {
                    if (x < 0 || y < 0 || x >= cols || y >= rows || x == midX && y == midY)
                    {
                        continue;
                    }

                    total += grid[x, y] == '#' ? 1 : 0;
                }
            }

            return total;
        }

        private int NumberOfAdjacentOccupied_B(int midX, int midY, char[,] grid)
        {
            var cols = grid.GetLength(0);
            var rows = grid.GetLength(1);

            var total = 0;
            foreach (var direction in Direction.AllDirections)
            {
                var x = midX;
                var y = midY;
                while (true)
                {
                    (x, y) = direction.Move(x, y);

                    if (x < 0 || y < 0 || x >= cols || y >= rows)
                    {
                        break;
                    }

                    if (grid[x, y] == '#')
                    {
                        total++;
                        break;
                    }

                    if (grid[x, y] == 'L')
                    {
                        break;
                    }
                }
            }

            return total;
        }

        private class Direction
        {
            private static readonly Direction Up = new Direction(0, -1);
            private static readonly Direction UpRight = new Direction(1, -1);
            private static readonly Direction Right = new Direction(1, 0);
            private static readonly Direction DownRight = new Direction(1, 1);
            private static readonly Direction Down = new Direction(0, 1);
            private static readonly Direction DownLeft = new Direction(-1, 1);
            private static readonly Direction Left = new Direction(-1, 0);
            private static readonly Direction UpLeft = new Direction(-1, -1);

            public static readonly IEnumerable<Direction> AllDirections = new[] {Up, UpRight, Right, DownRight, Down, DownLeft, Left, UpLeft};

            private readonly int dx;
            private readonly int dy;

            private Direction(int dx, int dy)
            {
                this.dx = dx;
                this.dy = dy;
            }

            public (int, int) Move(int x, int y)
            {
                return (x + dx, y + dy);
            }
        }
    }
}
