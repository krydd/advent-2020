using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Advent_2020.Tasks
{
    public class Day2
    {
        public void Go_A()
        {
            var lines = File.ReadAllLines("Tasks\\Data\\day2.txt");

            var count = lines.Count(ValidPassword_Part1);

            Console.WriteLine($"Ans: {count}");
        }
        
        public void Go_B()
        {
            var lines = File.ReadAllLines("Tasks\\Data\\day2.txt");

            var count = lines.Count(ValidPassword_Part2);

            Console.WriteLine($"Ans: {count}");
        }
        
        private bool ValidPassword_Part1(string line)
        {
            var parts = line.Split(":");
            var password = parts[1].Trim();
            var result = Regex.Match(parts[0].Trim(), @"(\d+)-(\d+) (\w)");

            var min = int.Parse(result.Groups[1].Value);
            var max = int.Parse(result.Groups[2].Value);
            var character = result.Groups[3].Value.ToCharArray().First();

            var count = password.Count(c => c == character);
            return count >= min && count <= max;
        }

        private bool ValidPassword_Part2(string line)
        {
            var parts = line.Split(":");
            var password = parts[1].Trim().ToCharArray();
            var result = Regex.Match(parts[0].Trim(), @"(\d+)-(\d+) (\w)");

            var pos1 = int.Parse(result.Groups[1].Value);
            var pos2 = int.Parse(result.Groups[2].Value);
            var character = result.Groups[3].Value.ToCharArray().First();

            return (password[pos1 - 1] == character ? 1 : 0) + (password[pos2 - 1] == character ? 1 : 0) == 1;
        }
    }
}
